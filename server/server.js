const express = require('express');
const bodyParser = require('body-parser');
const Sequalize = require('sequelize'); //dependinte
const path = require('path');
const PORT = 8080;
const TOTAL_TICKETS = 1100;
const connect = new Sequalize("bal", "root", "root", {
  dialect: "mysql",
  define: {
    timestamps: false
  }
})//conexiunea cu baza de date

const Ticket = connect.define("ticket", {

  totalTickets: Sequalize.INTEGER,
  soldTickets: Sequalize.INTEGER
})//campurile din baza de date

const app = express();
app.use(bodyParser.json())

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../.', 'client', 'index.html'));
});
app.use(express.static(path.resolve(__dirname, '..', 'client')));

app.post("/api/create", (request, response, next) => {

  if (request.body.force === "admin_password") {
    connect.sync({
      force: true
    }).then(() => Ticket.create({
      totalTickets: TOTAL_TICKETS,
      soldTickets: 0
    }).then(() => response.status(201).send("Created")))
  }
  else {
    response.status(403).send("Forbidden");
  }


})


app.get("/api/tickets", (request, response, next) => {
  response.header('Cache-Control', 'no-cache, no-store');
  Ticket.findOne().then(ticket => {
    response.status(200).send(ticket)
  }).catch(() => response.status(404).send("Not found"))
})

app.post("/api/croco", (req, res, next) => {
  if (req.body.check === "sarcos1")
    next();
  else {
    res.status(403).send({message: "Forbidden"});
  }
}, (request, response) => {
  Ticket.findOne().then(ticket => {
    if (ticket) {
      return ticket.update({
        soldTickets: Number(ticket.soldTickets) + Number(request.body.soldTickets)
      }).then(() => response.status(201).send(ticket))
    }

  })
})
app.post("/api/xcroco", (req, res, next) => {
  if (req.body.check === "sarcos1")
    next();
  else {
    res.status(403).send({message: "Forbidden"});
  }
}, (request, response) => {
  Ticket.findOne().then(ticket => {
    if (ticket) {
      return ticket.update({
        soldTickets: Number(request.body.soldTickets)
      }).then(() => response.status(201).send(ticket))
    }

  })
})

app.post("/api/check", (request, response) => {
  if (request.body.check === "sarcos1") response.status(200).send({message: "Access granted"});
  else {
    response.status(403).send({message: "Forbidden"});
  }

})

app.listen(PORT, () => {
  console.log(`server is running on http://localhost:${PORT}.`);
})
