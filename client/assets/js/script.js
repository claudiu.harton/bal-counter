
document.getElementById("send").addEventListener("click", function () {
    toastr.remove();
    const tickets = document.querySelector('#tickets').value;

    if (tickets.match('^[1-9]\\d*$')) {
        axios.post('/api/croco', {
            soldTickets: tickets,
          check:password
        }).then(response=>{
            toastr.success(`Tickets sold: ${response.data.soldTickets}.`);
            toastr.success(`Tickets left: ${Number(response.data.totalTickets) - Number(response.data.soldTickets)}.`);
        }).catch(error =>{
            toastr.error(error);
        })

        document.querySelector('#tickets').value = '';
    }else{
        toastr.error('Invalid number of tickets!')
    }
});

document.querySelector('#tickets').addEventListener('keypress', function (e) {
  let key = e.keyCode;
  if (key === 13) { // 13 is enter
    toastr.remove();
    const tickets = document.querySelector('#tickets').value;

    if (tickets.match('^[1-9]\\d*$')) {
      axios.post('/api/croco', {
        soldTickets: tickets,
        check:password
      }).then(response=>{
        toastr.success(`Tickets sold: ${response.data.soldTickets}.`);
        toastr.success(`Tickets left: ${Number(response.data.totalTickets) - Number(response.data.soldTickets)}.`);
      }).catch(error =>{
        toastr.error(error);
      })

      document.querySelector('#tickets').value = '';
    }else{
      toastr.error('Invalid number of tickets!')
    }
  }
});

let password;

document.getElementById("passwordSet").addEventListener("click", function () {
  toastr.remove();
  password = document.querySelector('#password').value;
  axios.post('/api/check', {
    check: password
  }).then(response => {
    toastr.success(response.data.message);
    document.querySelector('#passwordForm').setAttribute('style', 'display:none');
    document.querySelector('.forms').setAttribute('style', 'display:inherit');
  }).catch(error => {
    console.log(error)
    toastr.error(error.response.data.message);
  })
});

document.querySelector('#password').addEventListener('keypress', function (e) {
  let key = e.keyCode;
  if (key === 13) { // 13 is enter
    toastr.remove();
    password = document.querySelector('#password').value;
    axios.post('/api/check', {
      check: password
    }).then(response => {
      toastr.success(response.data.message);
      document.querySelector('#passwordForm').setAttribute('style', 'display:none');
      document.querySelector('.forms').setAttribute('style', 'display:inherit');
    }).catch(error => {
      console.log(error)
      toastr.error(error.response.data.message);
    })
  }
});


document.getElementById("resetButton").addEventListener("click", function () {
    document.querySelector('#resetButton').setAttribute('style', 'display:none');
    document.querySelector('#resetForm').setAttribute('style', 'display:inherit');
})

document.getElementById("resetSend").addEventListener("click", function () {
    toastr.remove();
    const tickets = document.querySelector('#resetTickets').value;

    if (tickets.match('^[0-9]\\d*$')) {
        axios.post('/api/xcroco', {
            soldTickets: tickets,
            check:password
        }).then(response=>{

            toastr.success(`Tickets sold: ${response.data.soldTickets}.`);
            toastr.success(`Tickets left: ${Number(response.data.totalTickets) - Number(response.data.soldTickets)}.`);
        }).catch(error =>{
            toastr.error(error);
        })

        document.querySelector('#resetTickets').value = '';
        document.querySelector('#resetForm').setAttribute('style', 'display:none');

        document.querySelector('#resetButton').setAttribute('style', 'display:inherit');
    }else{
        toastr.error('Invalid number of tickets!')
    }
});

document.querySelector('#resetTickets').addEventListener('keypress', function (e) {
  let key = e.keyCode;
  if (key === 13) { // 13 is enter
    toastr.remove();
    const tickets = document.querySelector('#resetTickets').value;

    if (tickets.match('^[0-9]\\d*$')) {
      axios.post('/api/xcroco', {
        soldTickets: tickets,
        check:password
      }).then(response=>{

        toastr.success(`Tickets sold: ${response.data.soldTickets}.`);
        toastr.success(`Tickets left: ${Number(response.data.totalTickets) - Number(response.data.soldTickets)}.`);
      }).catch(error =>{
        toastr.error(error);
      })

      document.querySelector('#resetTickets').value = '';
      document.querySelector('#resetForm').setAttribute('style', 'display:none');

      document.querySelector('#resetButton').setAttribute('style', 'display:inherit');
    }else{
      toastr.error('Invalid number of tickets!')
    }
  }
});